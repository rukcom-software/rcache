<?php
  /*
    Plugin Name: RCache By Ruk-Com
    Plugin URI: https://help.ruk-com.in.th/topic/7773/
    Description: High performance caching management.
    Version: 1.6.1
    Author: Ruk-Com Co.,Ltd
    Author URI: https://ruk-com.cloud/
  */
  // plugin update checker
  require('vendor/plugin-update-checker/plugin-update-checker.php');
  $myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
    'https://gitlab.com/rukcom-software/rcache/',
    __FILE__,
    'rcache'
  );
  
  if ((float)phpversion() >= 7.0 && (float)phpversion() <= 7.1) {
    include_once("rukcom-cache-71.php");
  } elseif ((float)phpversion() >= 7.2 && (float)phpversion() <= 7.4) {
    include_once("rukcom-cache-72.php");
  } elseif ((float)phpversion() >= 8.0 && (float)phpversion() <= 8.1) {
    include_once("rukcom-cache-80.php");
  } elseif ((float)phpversion() >= 8.2) {
    include_once("rukcom-cache-82.php");
  } else {
    include_once("rukcom-cache-56.php");
  }
?>